Firing range

Make 2 hostage and 2 guards invincible
Wait for player to be outside firing range, and on PA additionally check for objective 0 or flag 1 or flag 22
Assign functions to 2 guards
Wait 10 seconds after door opening, or immediately after player enters firing range
Break glass and unset invincible