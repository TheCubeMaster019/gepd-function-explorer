4 guards invincibility

Wait for flag 21
Make 4 guards invincible
Wait for flag 31 (diversion objective completed)
Wait 5 seconds
Make 4 guards vulnerable

The invincible thing probably applies to gunfire only, which is why the mine/bombspy works.