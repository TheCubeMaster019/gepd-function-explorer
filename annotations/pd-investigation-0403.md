Scientist in firing room

Wait until player in certain room
If $this hasn't detected player, 1 in 256 chance of doing a random animation - probably an idle animation?
When detected player, say something and jog to pad 0133
Wait until not moving
Do some animation repeatedly