Strange function

Wait until within firing range or 3 seconds have elapsed.

If within firing range, run to Bond and attack.
If 3 seconds have elapsed:

Use RNG to determine distance
If actor within distance of $this->target_pad, run to Bond and attack

Seems like a bizarre function. Looks to be unused.