Trev at location A

Where Trev can go from each location:

A -> B, C, D, E, F
B -> A, C, D, E, G
C -> A, B, D, E
D -> A, B, C, E
E -> A, B, C, D
F -> A, B
G -> A, B

A is pad 90 (console room)
B is pad 91 (opposite console room)
C is pad 01 (top area 2)
D is pad 09 (top area 1)
E is pad 13 (top area 3)
F is pad 5C (down ramp from console room)
G is pad 4F (down ramp from opposite console room)