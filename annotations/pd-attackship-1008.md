Hanger exit lifts

Wait for enough kills in hanger
Activate lift
Wait for lift to arrive
Open doors
Disable lift
Wait until Jo in lift
Activate lift (5 second pause can happen because the lift would normally wait 5 seconds before moving)
Wait until Y >= 400
Wait until lift arrived
If Elvis's Y < 400:
    Move Elvis to pad AD, 01E6 or 01E7
End if
Activate Elvis's lift
Wait 2 seconds
Wait for lift to arrive
Assign function 0409 to Elvis
Disable Elvis's lift
If Elvis's Y is still < 400:
    Assign function 0427 to Elvis
    Move Elvis to pad 12
    Assign function 0428 to Elvis
End if
Wait until one of four objects activated (doors at bottom of lift, in case player was left behind?)
Play a sound
Activate Elvis's lift
Wait for lift to arrive
Wait for lift to be moving
Wait for lift to arrive