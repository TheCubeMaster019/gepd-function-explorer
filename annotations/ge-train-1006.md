End level

Wait until within 50 units of pad CB
Wait until any key pressed (3D) or three seconds passed (01)

Any key pressed (3D):
idle()

Three seconds passed (01):
Start fade out
If Natalya is not dead, complete objective
Wait for fade to complete
If Natalya dead/dying, exit
If an objective failed, exit
Stop timer
Advance frame