Teleport Joanna to Agent megaweapon area

I suspect Elvis can get stuck in the first area if he is injured while Jo is teleporting, because when he is injured he is reassigned a function.

Function 0402 (Elvis general) sets the onshot function to 0402
This function reassigns Elvis to 042c idle, which makes him stop moving and unassign the idle function
Frame is advanced
Elvis is moved to pad

Nothing obvious here, but maybe there's a programming mistake where the move actor to pad instruction refuses if the actor is busy, but follows the if by mistake?