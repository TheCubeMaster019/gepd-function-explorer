Hallway light switch

Between the first two rooms

Wait for switch to be activated
Set flag 8
Play noise
Adjust lights
Wait 2 seconds
Adjust lights

If activated a second time (flag 8 is already true):
Same kind of thing, but unset flag 8 so it toggles