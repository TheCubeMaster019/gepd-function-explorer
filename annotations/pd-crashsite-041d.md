President running

Main:
If shot, enter injuredinit(), else enter waitinit()

injuredinit() (08):
Start cycle counter
Play a sound

injuredloop() (9E):
Wait until not moving
Enter waitinit()

waitinit() (9D)
stop moving
restart cycle counter

waitloop() (9F):
If in room 0x14, goto A5 (run to ufo)
If player in sight, goto wait-08
If player not in sight, enter followinit() (A0)

wait-08:
If been in wait() for 20 seconds, waitinit()
If within 20 units of jo, waitloop()
followinit()

followinit() (A0):
Start cycle counter
If with 30 units of Jo:
    Start running to Jo with hand covering head
Else
	Start running to Jo normally
followloop()

followloop (A1):
If in room 0x14, goto A5 (run to ufo)
If within 10 units of player, waitinit()
If stopped moving, waitinit()
If timer reached 6 seconds, followinit()

Run to ufo (A5):
Run to pad
Wait until stopped
Set objective complete