Spawning

An actor who has this function will have their $actor->pad set to some value
When Jo is within 30 units of $actor->pad, a new actor will be spawned, up to 3 times
Could be the mini-Skedar near the floor switch?