X-ray scanning

Wait for using X-ray scanner and flag 6 to be set
Show "Scanner target acquired"
Wait 4 seconds, or until flag 6 is true (which it would be...?)

If flag 6 is true:
"Scanner lock lost"

If 4 seconds reached:
"Target ID confirmed - XT origin"
Set flag 25