Mini Skedar

Wait for flag 20, then start spawning mini-Skedar in one of three pads randomly until the end of the level?
Flag 18 determines frequency: If unset then every 8 seconds, if set then every 48 seconds.