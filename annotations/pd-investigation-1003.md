Bot programming terminal

SA/PA only:
Wait for terminal to be activated
If objective is failed, show "Maintenance robot system offline"
If objective is completed, show "Operation not allowed - robots busy"
If bot is on routine cleaning cycle, show "Operation not allowed - robots busy"
Else:
- Show "Maintenance robots reprogrammed" and set flag 11 (reprogrammed)
- If bot is currently active, show "Routine cleaning cycle activated" and set flag 30 (routine cleaning active)