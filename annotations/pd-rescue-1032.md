Ending keycard logic

The game tries to keep a counter to know how many people you've killed at the end, but if you kill the 0x23 guard first, the counter will continue to iterate on each frame because the guard is dead.