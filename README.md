# GoldenEye and Perfect Dark Function Explorer

This repository contains the tools used to generate the [GE/PD Function Explorer](https://ryandwyer.gitlab.io/gepd-function-explorer/index.html) pages.

The tools are just a couple of Python 3 libraries. They work with all known versions of the GE and PD ROMs, including betas. The GE/PD Function Explorer pages are generated using the JSON files present in this repository. The more curious of you may want to examine the PAL or JAP ROMs and the betas to look for differences.

The convertor works with a ROM filename passed as an argument to the convertor script.

The ROMs themselves are not included in this repository.

## Where is your instruction list?

Look in [GeRegistry.py](extractor/lib/GeRegistry.py) and [PdRegistry.py](extractor/lib/PdRegistry.py). These files also contain much more than just the instructions, such as actor, object and weapon lists.

## How do I submit an annotation?

Make the appropriate change in the `annotations` directory, then submit a pull request.

The format of an annotation file is pretty simple. The first line is the title, and all subsequent lines are the comments.

## How do I run the convertor?

Your command should be something like:

    $ python extractor/romToJson.py path/to/GoldenEye.z64

This will generate a JSON file in the `extractor` directory that can be used with the mkpages script. The name of the JSON file will be automatically determined.

Note that your ROM must not be byteflipped (the first 4 bytes should be `0x80371240`).

## How do I generate the HTML pages after making code changes?

You can run this:

    $ python extractor/mkpages.py
#### This will take a LONG TIME!
You can also generate the pages for just one game, which saves time:

    $ python extractor/mkpages.py geUSF

This will populate the `public` directory with HTML files, which you can open directly from disk (you don't need to set up a web server).

This script picks up the functions from the JSON files in the `extractor` directory. This is what's executed by GitLab - the functions are only stored this way because we can't commit the ROMs to the repository for legal reasons.

## What is the naming scheme for the JSON files?

The JSON files are named this way: xxYYZ
* `xx`: "ge" or "pd" for GoldenEye or Perfect Dark, respectively.
* `YY`: "US", "EU", or "JP" for NTSC-U, PAL, or NTSC-J, respectively.
* `Z`: "F", "B", or "R" for Final, Beta, or Release, respectively. Note that Release is only used for the Perfect Dark initial release ROM.

## How do I fix a bug or implement an unknown instruction?

All the good stuff is in [GeRegistry.py](extractor/lib/GeRegistry.py), [PdRegistry.py](extractor/lib/PdRegistry.py) and [the main parser](extractor/lib/Parser.py). Make a pull request.

