import lib.GeRegistry
import lib.PdRegistry
import re
from itertools import groupby

def load(game):
    if game == 'ge':
        return GeParser()
    else:
        return PdParser()

class Parser:
    function = None

    def setFunction(self, function):
        self.function = function
        return self

    def parse(self):
        instructions = []
        buffer = self.function['raw']
        type_len = self.TYPE_LENGTH
        type_id = None
        while type_id != 0x04 and len(buffer) != 0:
            type_id = int.from_bytes(buffer[0:type_len], 'big')
            try:
                params_len = self.getParamsLength(type_id, buffer)
                params = buffer[type_len:type_len+params_len]
                instructions.append({
                    'type': type_id,
                    'params': params,
                    'description': self.getParsedDescription(type_id, params),
                })
            except Exception as e:
                print('stage %d function %04x type %04x' % (self.function['stage_id'], self.function['id'], type_id))
                raise e
            buffer = buffer[type_len+params_len:]
        return instructions

    def getParamsLength(self, type_id, buffer):
        if self.isComment(type_id):
            return buffer[self.TYPE_LENGTH:].index(0) + 1

        type_string = self.typeIdToString(type_id)
        for key in self.registry.instructions:
            if key.startswith(type_string):
                return int(len(key) / 2) - self.TYPE_LENGTH

        raise Exception('Unrecognised instruction type %04x' % type_id)

    def isComment(self, type_id):
        return type_id == self.COMMENT_TYPE_ID

    def getParsedDescription(self, type_id, params):
        description = self.registry.instructions[self.getKey(type_id)]
        params = self.parseParams(type_id, params)
        if not isinstance(description, str):
            description = description(params)

        description = re.sub(r'{actor:([a-z])}',              lambda m: self.actor(params[m.group(1)]), description)
        description = re.sub(r'{actorprop:([0-9]):([a-z])}',  lambda m: self.actorprop(int(m.group(1)), params[m.group(2)]), description)
        description = re.sub(r'{ammotype:([a-z])}',           lambda m: self.ammotype(params[m.group(1)]), description)
        description = re.sub(r'{animation:([a-z])}',          lambda m: self.animation(params[m.group(1)]), description)
        description = re.sub(r'{dec:([a-z])}',                lambda m: self.dec(params[m.group(1)]), description)
        description = re.sub(r'{difficulty:([a-z])}',         lambda m: self.difficulty(params[m.group(1)]), description)
        description = re.sub(r'{doorstate:([a-z])}',          lambda m: self.doorstate(params[m.group(1)]), description)
        description = re.sub(r'{flag:([a-z])}',               lambda m: self.flag(params[m.group(1)]), description)
        description = re.sub(r'{function:([a-z])}',           lambda m: self.func(params[m.group(1)]), description)
        description = re.sub(r'{hex:([a-z])}',                lambda m: self.hex(params[m.group(1)]), description)
        description = re.sub(r'{object:([a-z])}',             lambda m: self.object(params[m.group(1)]), description)
        description = re.sub(r'{objective:([a-z])}',          lambda m: self.objective(params[m.group(1)]), description)
        description = re.sub(r'{objectprop:([0-9]):([a-z])}', lambda m: self.objectprop(int(m.group(1)), params[m.group(2)]), description)
        description = re.sub(r'{operator:([a-z])}',           lambda m: self.operator(params[m.group(1)]), description)
        description = re.sub(r'{pad:([a-z])}',                lambda m: self.pad(params[m.group(1)]), description)
        description = re.sub(r'{quip:([a-z]):([a-z])}',       lambda m: self.quip(params[m.group(1)], params[m.group(2)]), description)
        description = re.sub(r'{signed:([a-z])}',             lambda m: self.signed(params[m.group(1)]), description)
        description = re.sub(r'{secs:([a-z])}',               lambda m: self.secs(params[m.group(1)]), description)
        description = re.sub(r'{speed:([a-z])}',              lambda m: self.speed(params[m.group(1)]), description)
        description = re.sub(r'{stage:([a-z])}',              lambda m: self.stage(params[m.group(1)]), description)
        description = re.sub(r'{str:([a-z]+)}',               lambda m: self.str(params[m.group(1)]), description)
        description = re.sub(r'{text:([a-z])}',               lambda m: self.text(params[m.group(1)]), description)
        description = re.sub(r'{thing:([a-z]):([a-z])}',      lambda m: self.thing(params[m.group(1)], params[m.group(2)]), description)
        description = re.sub(r'{weapon:([a-z])}',             lambda m: self.weapon(params[m.group(1)]), description)

        return description

    def getKey(self, type_id):
        type_string = self.typeIdToString(type_id)
        for key in self.registry.instructions:
            if key.startswith(type_string):
                return key

    def parseParams(self, type_id, params):
        param_object = {'raw': params}
        fmt = self.getKey(type_id)[self.TYPE_LENGTH*2:]
        pos = 0
        for char, chars in groupby(fmt):
            bytes_len = int(len(list(chars)) / 2)
            param_object[char] = params[pos:pos+bytes_len]
            pos += bytes_len
        return param_object

    """
    Parser callbacks below
    """

    def actor(self, value):
        value = int.from_bytes(value, 'big')
        if value in self.registry.actors:
            return self.registry.actors[value]
        try:
            return 'actor %02x (%s)' % (value, self.registry.stage_specific[self.function['stage_id']]['actors'][value]['name'])
        except:
            return 'actor %02x' % value

    def actorprop(self, bank, prop):
        prop = int.from_bytes(prop, 'big')
        if bank in self.registry.actorprops and prop in self.registry.actorprops[bank]:
            return '%s:%08x (%s)' % (bank, prop, self.registry.actorprops[bank][prop])
        return '%d:%08x' % (bank, prop)

    def ammotype(self, value):
        value = int.from_bytes(value, 'big')
        return '%02x (%s)' % (value, self.registry.ammotypes[value])

    def animation(self, value):
        value = int.from_bytes(value, 'big')
        if value in self.registry.animations:
            return '%04x (%s)' % (value, self.registry.animations[value])
        return '%04x' % value

    def dec(self, value):
        return str(int.from_bytes(value, 'big'))

    def difficulty(self, value):
        value = int.from_bytes(value, 'big')
        try:
            return self.DIFFICULTIES[value]
        except:
            return '%02x' % value

    def doorstate(self, value):
        value = int.from_bytes(value, 'big')
        states = []
        if value & 1: states.append('closed')
        if value & 2: states.append('open')
        if value & 4: states.append('closing')
        if value & 8: states.append('opening')
        return ' or '.join(states);

    def flag(self, value):
        value = int.from_bytes(value, 'big')
        index = 0
        while (1 << index) & value == 0:
            index += 1
        index += 1
        try:
            return 'flag #%d (%s)' % (index, self.registry.stage_specific[self.function['stage_id']]['flags'][index])
        except:
            return 'flag #%d' % index

    def func(self, value):
        return '[%04x]' % int.from_bytes(value, 'big')

    def hex(self, value):
        value = int.from_bytes(value, 'big')
        if value <= 255:
            return '%02x' % value
        return '%04x' % value

    def object(self, value):
        value = int.from_bytes(value, 'big')
        try:
            return '%02x (%s)' % (value, self.registry.stage_specific[self.function['stage_id']]['objects'][value]['name'])
        except:
            return '%02x' % value

    def objective(self, value):
        value = int.from_bytes(value, 'big')
        try:
            return 'objective "%s"' % self.registry.stage_specific[self.function['stage_id']]['objectves'][value]
        except:
            return 'objective #%d' % (value + 1)

    def objectprop(self, bank, prop):
        prop = int.from_bytes(prop, 'big')
        if bank in self.registry.objectprops and prop in self.registry.objectprops[bank]:
            return '%s:%08x (%s)' % (bank, prop, self.registry.objectprops[bank][prop])
        return '%d:%08x' % (bank, prop)

    def operator(self, value):
        value = int.from_bytes(value, 'big')
        return '>' if value else '<'

    def pad(self, value):
        value = int.from_bytes(value, 'big')
        if value == 0x2328:
            return '$self->pad'
        return 'pad %04x' % value

    def quip(self, bank, set):
        bank = int.from_bytes(bank, 'big')
        set = int.from_bytes(set, 'big')
        try:
            return '"%s"' % '","'.join(self.registry.quips[bank][set])
        except:
            return 'unknown quip'

    def revoperator(self, value):
        value = int.from_bytes(value, 'big')
        return '<' if value else '>'

    def secs(self, value):
        value = int.from_bytes(value, 'big')
        return '%.1f' % (value / 60);

    def speed(self, value):
        value = int.from_bytes(value, 'big')
        return ['slow','medium','fast'][value]

    def stage(self, value):
        value = int.from_bytes(value, 'big')
        return self.registry.stages[value]

    def signed(self, value):
        return str(int.from_bytes(value, 'big', signed=True))

    def str(self, value):
        return value[:-1].decode('utf-8').strip()

    def text(self, value):
        value = int.from_bytes(value, 'big')
        try:
            return self.registry.texts[value].strip()
        except:
            return 'unknown text'

    def thing(self, b_entity_type, b_entity_id):
        entity_type = int.from_bytes(b_entity_type, 'big')
        entity_id = int.from_bytes(b_entity_id, 'big')
        if entity_type == 0x0001:
            return 'player 1 (?)'
        if entity_type == 0x0002:
            return '$player'
        if entity_type == 0x0004:
            return self.actor(b_entity_id)
        if entity_type == 0x0008:
            return self.pad(b_entity_id)
        return 'something (entity type=0x%04x, entity_id=0x%04x)' % (entity_type, entity_id)

    def weapon(self, value):
        value = int.from_bytes(value, 'big')
        if value in self.registry.weapons:
            return self.registry.weapons[value]
        return 'weapon %02x' % value

class GeParser(Parser):
    GAME = 'ge'
    TYPE_LENGTH = 1
    COMMENT_TYPE_ID = 0xad
    DIFFICULTIES = ['A','SA','00A','007']
    registry = lib.GeRegistry

    def typeIdToString(self, type_id):
        return '%02x' % type_id

class PdParser(Parser):
    GAME = 'pd'
    TYPE_LENGTH = 2
    COMMENT_TYPE_ID = 0x00b5
    DIFFICULTIES = ['A','SA','PA','PD']
    registry = lib.PdRegistry

    def typeIdToString(self, type_id):
        return '%04x' % type_id
