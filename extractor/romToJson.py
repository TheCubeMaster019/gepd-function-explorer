import sys, lib.HtmlGenerator, lib.Parser, lib.Rom, json, base64

if len(sys.argv) == 1:
    print("Usage: python romToJson.py <rom-filename>")
    exit(1)

"""
Step 1: The Rom library reads the functions out of a ROM and returns them as an
array of dicts. Each dict contains a stage_id, function_id (eg. 0x0401) and a
'raw' element which is the entire function as a block of bytes.
"""
rom = lib.Rom.load(sys.argv[1])
functions = rom.getFunctions()

for function in functions:
    function['raw'] = base64.encodebytes(function['raw']).decode('utf-8').replace('\n', '')

"""
Step 2: Generate a JSON file to use with mkpages.py.
"""

fp = open('extractor/%s%s.json' % (rom.GAME, rom.VERSION), 'w')
json.dump(functions, fp, indent=4)
fp.close()
